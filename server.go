package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/mail"
	"sort"

	"github.com/emersion/go-smtp"
	"github.com/jhillyerd/enmime"
	"github.com/spf13/viper"
)

type message struct {
	From       *mail.Address
	Recipients []*mail.Address
	TO         []*mail.Address
	CC         []*mail.Address
	BCC        []*mail.Address
	Data       []byte
	Envelope   *enmime.Envelope
	Opts       *smtp.MailOptions
}

func (m message) GetSubject() string {
	return m.Envelope.GetHeader("Subject")
}

type sender interface {
	Send(m message) error
}

// The backend implements SMTP server methods.
type backend struct {
	messages []*message
	anonmsgs []*message

	// Errors returned by Data method.
	dataErrors chan error

	// Error that will be returned by Data method.
	dataErr error

	// Read N bytes of message before returning dataErr.
	dataErrOffset int64

	panicOnMail bool
	userErr     error

	sender
}

// Login handles a login command with username and password.
//goland:noinspection GoUnusedParameter
func (bkd *backend) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	// TODO: Check username and password.
	// if username != "username" || password != "password" {
	//	return nil, errors.New("invalid username or password")
	// }
	return &session{backend: bkd, Connection: state}, nil
}

// AnonymousLogin requires clients to authenticate using SMTP AUTH before sending emails
func (bkd *backend) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	// return nil, smtp.ErrAuthRequired
	return &session{backend: bkd, Connection: state}, nil
}

// A session is returned after successful login.
type session struct {
	backend   *backend
	anonymous bool

	Connection *smtp.ConnectionState

	msg *message
}

func (s *session) Mail(from string, opts smtp.MailOptions) error {
	if s.backend.panicOnMail {
		panic("Everything is on fire!")
	}
	s.Reset()

	log.Println("New message received!")
	fromSplit, err := mail.ParseAddress(from)
	if err != nil {
		return fmt.Errorf("invalid from address (%v): %w", from, err)
	}
	s.msg.From = fromSplit
	s.msg.Opts = &opts
	log.Println("Mail from: ", from, "@", s.Connection.RemoteAddr, "/", s.Connection.Hostname)
	return nil
}

func (s *session) Rcpt(to string) error {
	toSplit, err := mail.ParseAddress(to)
	if err != nil {
		return fmt.Errorf("invalid to address (%v): %w", to, err)
	}
	s.msg.Recipients = append(s.msg.Recipients, toSplit)
	log.Println("Rcpt to:", to)
	return nil
}

func (s *session) Data(r io.Reader) error {
	if s.backend.dataErr != nil {

		if s.backend.dataErrOffset != 0 {
			_, _ = io.CopyN(ioutil.Discard, r, s.backend.dataErrOffset)
		}

		err := s.backend.dataErr
		if s.backend.dataErrors != nil {
			s.backend.dataErrors <- err
		}
		return err
	}

	if b, err := ioutil.ReadAll(r); err != nil {
		if s.backend.dataErrors != nil {
			s.backend.dataErrors <- err
		}
		return err
	} else {
		s.msg.Data = b
		if s.anonymous {
			s.backend.anonmsgs = append(s.backend.anonmsgs, s.msg)
		} else {
			s.backend.messages = append(s.backend.messages, s.msg)
		}
		if s.backend.dataErrors != nil {
			s.backend.dataErrors <- nil
		}
		// log.Println("Data:\n", string(b))

		// Decode the message data into its MIME parts.
		if err := s.ReadEnvelope(); err != nil {
			return fmt.Errorf("failed to read envelope: %w", err)
		}

		// TODO: Optionally save message as .msg file

		log.Println("Forwarding message to API Service")
		if err := s.backend.sender.Send(*s.msg); err != nil {
			return fmt.Errorf("failed to send message: %w", err)
		}
		log.Println("Message forwarded to API Service successfully!")
	}
	return nil
}

func (s *session) Reset() {
	s.msg = &message{}
}

func (s *session) Logout() error {
	return nil
}

func (s *session) ReadEnvelope() error {
	// Parse message body with enmime.
	env, err := enmime.ReadEnvelope(bytes.NewReader(s.msg.Data))
	if err != nil {
		return fmt.Errorf("failed to parse email data: %w", err)
	}
	s.msg.Envelope = env

	if viper.GetBool(Debug) {
		log.Println("Decoding Message")
	}

	// A list of headers is retrieved via Envelope.GetHeaderKeys().
	headers := env.GetHeaderKeys()
	sort.Strings(headers)

	// Print each header, key and value.
	if viper.GetBool(Debug) {
		for _, header := range headers {
			log.Printf("%s: %v\n", header, env.GetHeader(header))
		}
	}

	// Headers can be retrieved via Envelope.GetHeader(name).
	log.Printf("From: %v\n", env.GetHeader("From"))

	// Address-type headers can be parsed into a list of decoded mail.Address structs.
	toList, _ := env.AddressList("To")
	for _, addr := range toList {
		s.msg.TO = append(s.msg.TO, addr)
		log.Printf("To: %s <%s>\n", addr.Name, addr.Address)
	}

	// Address-type headers can be parsed into a list of decoded mail.Address structs.
	ccList, _ := env.AddressList("CC")
	for _, addr := range ccList {
		s.msg.CC = append(s.msg.CC, addr)
		log.Printf("CC: %s <%s>\n", addr.Name, addr.Address)
	}

	// Populate the BCC addresses
	for _, addr := range s.msg.Recipients {
		if containsAddress(s.msg.TO, addr) {
			continue
		}
		if containsAddress(s.msg.CC, addr) {
			continue
		}
		s.msg.BCC = append(s.msg.BCC, addr)
		log.Printf("BCC: %s <%s>\n", addr.Name, addr.Address)
	}

	if viper.GetBool(Debug) {
		// enmime can decode quoted-printable headers.
		log.Printf("Subject: %v\n", env.GetHeader("Subject"))

		// The plain text body is available as mime.Text.
		log.Printf("Text Body: %v chars\n", len(env.Text))

		// The HTML body is stored in mime.HTML.
		log.Printf("HTML Body: %v chars\n", len(env.HTML))

		// mime.Inlines is a slice of inlined attachments.
		log.Printf("Inlines: %v\n", len(env.Inlines))

		// mime.Attachments contains the non-inline attachments.
		log.Printf("Attachments: %v\n", len(env.Attachments))
	}

	return nil
}

func containsAddress(s []*mail.Address, e *mail.Address) bool {
	for _, a := range s {
		if a.Address == e.Address {
			return true
		}
	}
	return false
}
