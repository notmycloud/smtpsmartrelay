FROM scratch
COPY smtpsmartrelay /usr/bin/smtpsmartrelay
ENTRYPOINT ["/usr/bin/smtpsmartrelay"]
