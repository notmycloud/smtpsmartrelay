# SMTP Smart Relay

Provides a SMTP that will route through a mail API service.
Initial support for SendGrid, others to follow as requested.

## Configuration Notes
Configuration can be set via Environmental Variables, Config File, or command line flags.

The config file should be named `smtpsmartrelay.yaml` and can be found in the following directories.
- /etc/smtpsmartrelay/
- ~/.config/smtpsmartrelay/
- Current directory (this can be changed using the `-config` flag.)

Flags are case sensitive and environment variables must be all uppercase and prefixed by `SMTP_SMART_RELAY`.
This prefix is omitted from the below table for formatting purposes.
An option with a period in the name designates a subkey in YAML format and periods and dashes should be replaced with underscored for environment variables.

Please reference `github.com/emersion/go-smtp` for more detail on the `server...` configuration options.

| ENV (Include Prefix of SMTP_SMART_RELAY)                                        | Flag                       | Config                            | Default                  | Description                                                                                                                                       |
|---------------------------------------------|----------------------------|-----------------------------------|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| _CONFIG                     | config                     | N/A                               | "."                      | Path to find the `smtpsmartrelay.yaml` configuration file                                                                                         |
| _DEBUG                      | debug                      | debug:                            | false                    | Enable debug logging                                                                                                                              |
| _SERVER_PORT                | server.port                | server:   port:                   | 2525                     | Port for the SMTP server to listen for new messages                                                                                               |
| _SERVER_DOMAIN              | server.domain              | server:   domain:                 | localhost                | SMTP relay domain, should match the public domain as configured in the relay service                                                              |
| _SERVER_READ_TIMEOUT        | server.read-timeout        | server:   read-timeout:           | 10s                      | Read timeout for incoming mail.                                                                                                                   |
| _SERVER_WRITE_TIMEOUT       | server.write-timeout       | server:   write-timeout:          | 10s                      | Write timeout for incoming mail.                                                                                                                  |
| _SERVER_MAX_MESSAGE_SIZE    | server.max-message-size    | server:   max-message-size:       | 25                       | Maximum size of an email message in Megabytes.                                                                                                    |
| _SERVER_MAX_RECIPIENTS      | server.max-recipients      | server:   max-recipients          | 10                       | Maximum number of recipients per email.                                                                                                           |
| _SERVER_ALLOW_INSECURE_AUTH | server.allow-insecure-auth | server:   allow-insecure-auth:    | false                    | Allow unencrypted authentication.                                                                                                                 |
| _SERVER_ENABLE_SASL_LOGIN   | server.enable-sasl-login   | server:   enable-sasl-login:      | false                    | Enable SASL based login. Per this [issue](https://github.com/emersion/go-smtp/issues/41) on the supporting package, LOGIN is obsolete.            |
| _SENDGRID_ENABLE            | sendgrid.enable            | sendgrid:   enable:               | true                     | Currently doesn't do anything, but will be added in the future when we enable additional API services. Only one service can be enabled at a time. |
| _SENDGRID_API_KEY           | sendgrid.api.key           | sendgrid:   api:     key:         | ""                       | Your private API key for the SendGrid API service.                                                                                                |
| _SENDGRID_API_HOST          | sendgrid.api.host          | sendgrid:   api:     host:        | https://api.sendgrid.com | API domain. (You should not need to change this.)                                                                                                 |
| _SENDGRID_API_ENDPOINT      | sendgrid.api.endpoint      | sendgrid:   api:     endpoint:    | /v3/mail/send            | API Endpoint. (You should not need to change this.)                                                                                               |
| _SENDGRID_OVERRIDE_NAME     | sendgrid.override.name     | sendgrid:   override:     name:   | false                    | Override the name of the email, will use the username from the original address.                                                                  |
| _SENDGRID_OVERRIDE_DOMAIN   | sendgrid.override.domain   | sendgrid:   override:     domain: | ""                       | Override the domain of the from address.                                                                                                          |
| _SENDGRID_OVERRIDE_FROM     | sendgrid.override.from     | sendgrid:   override:     from:   | ""                       | Override the from address in the email.                                                                                                           |

## Basic Configuration
In my case, I registered a subdomain with SendGrid so as to not interfere with my base domain email.
I registered `sg.domain.com` and they show `#####.sg.domain.com` on my website.
In this case, you need to override the domain to `sg.domain.com` or the API call will fail.
```
Docker run --name="sendgrid" \
        -e SMTP_SMART_RELAY_SERVER_DOMAIN=sg.domain.com \
        -e SMTP_SMART_RELAY_SERVER_ALLOW_INSECURE_AUTH=TRUE \
        -e SMTP_SMART_RELAY_SERVER_ENABLE_SASL_LOGIN=TRUE \
        -e SMTP_SMART_RELAY_SENDGRID_API_KEY=<SUPERSECRETAPIKEY> \
        -e SMTP_SMART_RELAY_SENDGRID_OVERRIDE_NAME=TRUE \
        -e SMTP_SMART_RELAY_SENDGRID_OVERRIDE_DOMAIN=sg.domain.com \
        -p 2525:2525 \
        -v /etc/localtime:/etc/localtime:ro \
        -v /etc/ssl/certs/:/etc/ssl/certs/:ro \
        registry.gitlab.com/notmycloud/smtpsmartrelay:latest
```
If you wanted to load the above via a configuration file it would look as follows.
```
server:
  domain: sg.domain.com
  allow-insecure-auth: true
  enable-sasl-login: true
sendgrid:
  api:
    key: <SUPERSECRETAPIKEY>
  override:
    name: true
    domain: sg.doamin.com
```
