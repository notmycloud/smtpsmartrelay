package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	EnvVarPrefix            = "SMTP_SMART_RELAY"
	DefaultSmtpPort         = 2525
	AppName                 = "smtpsmartrelay"
	ConfigFile              = "config"
	Debug                   = "debug"
	ServerPort              = "server.port"
	ServerDomain            = "server.domain"
	ServerReadTimeout       = "server.read-timeout"
	ServerWriteTimeout      = "server.write-timeout"
	ServerMaxMessageSize    = "server.max-message-size"
	ServerMaxRecipients     = "server.max-recipients"
	ServerAllowInsecureAuth = "server.allow-insecure-auth"
	ServerEnableSASLLogin   = "server.enable-sasl-login"
)

func init() {
	// Populate command line flags
	pflag.String(ConfigFile, ".", "Path to config file")
	pflag.Bool(Debug, false, "Enable debug mode")
	pflag.Int(ServerPort, DefaultSmtpPort, "Port to listen for new emails")
	pflag.String(ServerDomain, "localhost", "SMTP Relay domain, should match the public domain configured in the API service")
	pflag.Duration(ServerReadTimeout, 10*time.Second, "Read timeout for incoming mail")
	pflag.Duration(ServerWriteTimeout, 10*time.Second, "Write timeout for incoming mail")
	pflag.Int(ServerMaxMessageSize, 25, "Maximum size of an email message in Megabytes")
	pflag.Int(ServerMaxRecipients, 10, "Maximum number of recipients per email")
	pflag.Bool(ServerAllowInsecureAuth, false, "Allow insecure authentication")
	pflag.Bool(ServerEnableSASLLogin, false, "Enable SASL login")

	// Initialize API Servers
	sendGridInit()

	// Initialize the config
	pflag.Parse()

	viper.SetEnvPrefix(EnvVarPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))
	viper.AutomaticEnv()

	viper.SetConfigName(AppName)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(strings.Join([]string{"/etc", AppName}, string(os.PathSeparator)))
	viper.AddConfigPath(strings.Join([]string{"$HOME", ".config", AppName}, string(os.PathSeparator)))
	viper.AddConfigPath(pflag.Lookup(ConfigFile).Value.String())
	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			// return fmt.Errorf("failed to find config file: %w", err)
			// no-op
		} else {
			// Config file was found but another error was produced
			log.Panicf("failed to load config file: %v", err)
		}
	} else {
		log.Printf("Loaded config file: %s\n", viper.ConfigFileUsed())
	}

	err = viper.BindPFlags(pflag.CommandLine)
	if err != nil {
		log.Panicf("failed to parse command line flags: %v", err)
	}

	if viper.GetBool(Debug) {
		viper.Debug()
	}

}

func main() {
	be := &backend{}
	be.sender = new(sendGridServer)

	s := smtp.NewServer(be)

	s.Addr = ":" + viper.GetString(ServerPort)
	s.Domain = viper.GetString(ServerDomain)
	s.ReadTimeout = viper.GetDuration(ServerReadTimeout)
	s.WriteTimeout = viper.GetDuration(ServerWriteTimeout)
	s.MaxMessageBytes = 1024 * 1024 * viper.GetInt(ServerMaxMessageSize)
	s.MaxRecipients = viper.GetInt(ServerMaxRecipients)
	s.AllowInsecureAuth = viper.GetBool(ServerAllowInsecureAuth)

	// sasl.Login is depreciated and is only enabled for backwards compatibility.
	// https://github.com/emersion/go-smtp/issues/41
	if viper.GetBool(ServerEnableSASLLogin) {
		s.EnableAuth(sasl.Login, func(conn *smtp.Conn) sasl.Server {
			return sasl.NewLoginServer(func(username, password string) error {
				state := conn.State()
				session, err := be.Login(&state, username, password)
				if err != nil {
					return err
				}

				conn.SetSession(session)
				return nil
			})
		})
	}

	if viper.GetBool(Debug) {
		serverJSON, err := json.MarshalIndent(s, "", "  ")
		if err != nil {
			log.Fatalf(err.Error())
		}
		fmt.Printf("Server Configuration\n %s\n", string(serverJSON))
	}

	log.Println("Starting server at", s.Addr)
	if err := s.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
