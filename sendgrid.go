package main

import (
	"encoding/base64"
	"log"
	"strings"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	SendGridAPIKey      = "sendgrid.api.key"
	SendGridAPIHost     = "sendgrid.api.host"
	SendGridAPIEndpoint = "sendgrid.api.endpoint"

	SendGridOverrideName   = "sendgrid.override.name"
	SendGridOverrideDomain = "sendgrid.override.domain"
	SendGridOverrideFrom   = "sendgrid.override.from"
)

func sendGridInit() {
	pflag.String(SendGridAPIKey, "", "SendGrid API key")
	pflag.String(SendGridAPIHost, "https://api.sendgrid.com", "SendGrid API host")
	pflag.String(SendGridAPIEndpoint, "/v3/mail/send", "SendGrid API endpoint")
	// pflag.Int(SendGridAPIRateLimitMailMonthly, 6000, "SendGrid API Monthly Mail rate limit")
	// pflag.Int(SendGridAPIRateLimitMailDaily, 100, "SendGrid API Daily Mail rate limit")
	// pflag.Int(SendGridAPIRateLimitEndpoint, 600, "SendGrid API Minutely Endpoint rate limit")
	pflag.Bool(SendGridOverrideName, false, "Override the name of the email, will use the username from the original address")
	pflag.String(SendGridOverrideDomain, "", "Override the domain of the from address")
	pflag.String(SendGridOverrideFrom, "", "Override the from address in the email")
}

type sendGridServer struct {
}

func (s sendGridServer) SetFrom(name string, addr string) *mail.Email {
	var (
		newName string
		newAddr string
	)

	if viper.GetBool(SendGridOverrideName) {
		newName = strings.Split(addr, "@")[0]
		log.Printf("Overriding FROM name to %s\n", newName)
	} else {
		newName = name
	}

	if viper.IsSet(SendGridOverrideFrom) {
		newAddr = viper.GetString(SendGridOverrideFrom)
		log.Printf("Overriding FROM address to %s\n", newAddr)
	} else if viper.IsSet(SendGridOverrideDomain) {
		newAddr = strings.Split(addr, "@")[0] + "@" + viper.GetString(SendGridOverrideDomain)
		log.Printf("Overriding FROM address to %s\n", newAddr)
	} else {
		newAddr = addr
	}

	return mail.NewEmail(newName, newAddr)
}

func (s sendGridServer) Send(m message) error {
	message := mail.NewV3Mail()
	message.SetFrom(s.SetFrom(m.From.Name, m.From.Address))
	message.Subject = m.GetSubject()
	personalize := mail.NewPersonalization()
	for _, addr := range m.TO {
		personalize.AddTos(mail.NewEmail(addr.Name, addr.Address))
	}
	for _, addr := range m.CC {
		personalize.AddCCs(mail.NewEmail(addr.Name, addr.Address))
	}
	for _, addr := range m.BCC {
		personalize.AddBCCs(mail.NewEmail(addr.Name, addr.Address))
	}
	message.AddPersonalizations(personalize)
	if m.Envelope.Text != "" {
		message.AddContent(mail.NewContent("text/plain", m.Envelope.Text))
	}
	if m.Envelope.HTML != "" {
		message.AddContent(mail.NewContent("text/html", m.Envelope.HTML))
	}
	if m.Envelope.Inlines != nil {
		for _, attachment := range m.Envelope.Inlines {
			sgAttachment := mail.NewAttachment()
			sgAttachment.Filename = attachment.FileName
			sgAttachment.Type = attachment.ContentType
			sgAttachment.Disposition = attachment.Disposition
			sgAttachment.Content = base64.StdEncoding.EncodeToString(attachment.Content)
			message.AddAttachment(sgAttachment)
		}
	}
	if m.Envelope.Attachments != nil {
		for _, attachment := range m.Envelope.Attachments {
			sgAttachment := mail.NewAttachment()
			sgAttachment.Filename = attachment.FileName
			sgAttachment.Type = attachment.ContentType
			sgAttachment.Disposition = attachment.Disposition
			sgAttachment.Content = base64.StdEncoding.EncodeToString(attachment.Content)
			message.AddAttachment(sgAttachment)
		}
	}

	request := sendgrid.GetRequest(viper.GetString(SendGridAPIKey), viper.GetString(SendGridAPIEndpoint), viper.GetString(SendGridAPIHost))
	request.Method = "POST"
	request.Body = mail.GetRequestBody(message)
	response, err := sendgrid.API(request)
	if err != nil {
		log.Printf("Sendgrid API Error: %v\n", err)
	} else {
		log.Printf("Sendgrid API Status Code: %v\n", response.StatusCode)
		log.Printf("Sendgrid API Data: %v\n", response.Body)
		log.Printf("Sendgrid API Headers: %v\n", response.Headers)
	}
	return nil
}
